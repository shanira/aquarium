<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m180312_173216_create_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('user', [
          'id' => $this->primaryKey(),
          'first_name' => $this->string(),
          'last_name' => $this->string(),
          'username' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('user');
    }
}
