<?php

use yii\db\Migration;

/**
 * Handles the creation of table `container`.
 */
class m180312_191206_create_container_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('container', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'location' => $this->string(),
            'water_type' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('container');
    }
}
